<?php
use function LCB\did_cross_previous_path;
use PHPUnit\Framework\TestCase;

class TurtleFunctionTest extends TestCase
{
    /**
     * @dataProvider pathProvider
     * @param $path array Array defining turtle movement
     * @param $expected int Expected move to intersect previous path
     */
    public function testAdd($path, $expected)
    {
        $this->assertEquals($expected, did_cross_previous_path($path));
    }

    public function pathProvider()
    {
        return [
            [[1, 3, 2, 5, 4, 4, 6, 3, 2], 7],
            [[4, 4, 5, 7, 8, 6, 5, 6], 7],
            [[1, 2, 3, 4, 5, 6, 7], 0],
            [[2, 2, 4, 8, 6, 3, 3, 4, 2, 2], 8],
            [[5, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1, 1, 20], 12],
            [[6, 5, 3, 5, 4, 2], 4],
            [[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 3, 3, 4, 4], 17],
            [[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8], 0],
            [[1, 2, 3], 0],
            [[1, 1, 2, 1, 1, 2], 5],
        ];
    }
}