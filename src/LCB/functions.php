<?php

namespace LCB;

/**
 * @param array $path Array of moves which should be positive numbers.
 * @return int On which move did it cross previous path (Returns 0 if it didn't)
 */
function did_cross_previous_path(array $path)
{
    for ($position = 3; $position < count($path); ++$position) { // Can't intersect until fourth move
        if ($path[$position - 1] <= $path[$position - 3] // First condition is that previous path is equal or shorter than previous movement in opposite direction (3 moves before the current)
            && ( // In case it was, we can check all other conditions needed
                $path[$position] >= $path[$position - 2] // Second "basic" condition is that current line is also longe than previous movement in opposite direction (2 moves before the current). That means it formed rectangle with last 4 moves.
                || ( // Other possibility is that it intersected path from previous 4 movements. For that to happen, last 2 movements needs to be bigger than difference between previous 2 movements on that axis and movement 2 cycles before needs to be bigger than movement 4 cycles before (This intersection is happening by closing outwards going spiral)
                    $path[$position] >= $path[$position - 2] - ($path[$position - 4] ?? 0)
                    && $path[$position - 1] >= $path[$position - 3] - ($path[$position - 5] ?? 0)
                    && $path[$position - 2] >= ($path[$position - 4] ?? 0)
                )
            )) {
            return $position + 1; // returning array index incremented by one, to 1-index it.
        }
    }

    return 0; // returning 0 in case of no intersection
}