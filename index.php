<?php

use function LCB\did_cross_previous_path;

require_once 'vendor/autoload.php';

echo "With movements [1, 3, 2, 5, 4, 4, 6, 3, 2], turtle crossed previous path on move number: " .
     did_cross_previous_path([1, 3, 2, 5, 4, 4, 6, 3, 2]) . PHP_EOL;